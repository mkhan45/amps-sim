﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace UnityStandardAssets.Vehicles.Car
{
    [RequireComponent(typeof(CarController))]
public class TrainingData_collection_controller : MonoBehaviour
{
   
    public string recordingTargetFilePath = "targPath.csv";
    public bool recordingTargetPath = false;

    public int distBetweenWaypoints = 30;

    private waypoint target_path_waypoints_set_first;
    private waypoint target_path_waypoints_set_last;
  


    public class waypoint {
        public float x = 0;
        public float y = 0;
        public float z = 0;

        public waypoint next = null;

        public waypoint(float x1, float y1, float z1) {
            x = x1;
            y = y1;
            z = z1;
        }

        public waypoint(Vector3 pos) {
            x = pos.x;
            y = pos.y;
            z = pos.z;
        }

        public waypoint(string[] vals) {
            x = float.Parse(vals[0]);
            y = float.Parse(vals[1]);
            z = float.Parse(vals[2]);
        }

        public float[] getValues() {
            return new float[] {x,y,z};
        }

        public void setValues(float x1, float y1, float z1) {
            x = x1;
            y = y1;
            z = z1;
        }

        public void setValues(float[] vals) {
            x = vals[0];
            y = vals[1];
            z = vals[2];
        }

        public string to_str() {
            return "" + x + "," + y + "," + z;
        }
    }

    private void addWaypointToTargetSet(waypoint w) {
        if (target_path_waypoints_set_first == null) {
            target_path_waypoints_set_first = w;
            target_path_waypoints_set_last = w;
        }else{
            target_path_waypoints_set_last.next = w;
            target_path_waypoints_set_last = w;
        }
    }

      // Start is called before the first frame update
    void Start()
    {
        if (! recordingTargetPath) {
            using(var reader = new System.IO.StreamReader(recordingTargetFilePath) ) {
                int lineNum = 0;
                while (!reader.EndOfStream)
                {
                    string line = reader.ReadLine();
                    string[] vals = line.Split(',');

                    Debug.Assert(vals.Length == 3);

                    addWaypointToTargetSet(new waypoint(vals));

                    lineNum++;
                }
            }
        }
    }


    private float distance(waypoint v1, waypoint w1) {
        return Mathf.Sqrt( Mathf.Pow((v1.x-w1.x),2) + Mathf.Pow((v1.y-w1.y),2) + Mathf.Pow((v1.z-w1.z),2) );
    }
    
    // Update is called once per frame
    void Update()
    {
        if (recordingTargetPath) {
            addWaypointToTargetSet(new waypoint(transform.position));
        }       
    }


    void OnApplicationQuit()
    {
        if (recordingTargetPath) {
            var csv = new System.Text.StringBuilder();

      
            waypoint c_w = target_path_waypoints_set_first;
            csv.AppendLine(c_w.to_str());
            while (c_w.next != null) {
                c_w = c_w.next;
                csv.AppendLine(c_w.to_str());
            }

      
            System.IO.File.WriteAllText(recordingTargetFilePath, csv.ToString());
        }
    }


    public Vector3[] getNextWaypoints(Transform transform, int numWaypoints) {
        Vector3[] nxtWpts = new Vector3[numWaypoints];

        int wypts_indx = 0;
        int wypts_read = 0;
        waypoint c_w = getClosestWaypoint(transform);
        waypoint last = new waypoint(transform.position);
        while (c_w != null) {

            //add this waypoint to the return list if this waypoint is sufficiently far enough away from the last waypoint
            if (distance(last,c_w) > distBetweenWaypoints) {
                nxtWpts[wypts_indx] = new Vector3(c_w.x, c_w.y, c_w.z);
                wypts_indx++;
                last = c_w;
            }
            
            if (wypts_indx == numWaypoints) { // quit when we have all the waypoints
                break;
            }

            
            c_w = c_w.next;
            wypts_read++;
        }

        if (wypts_indx != numWaypoints) {
            Debug.Log("ERROR! Only got to " + wypts_indx + " waypoints after reading " + wypts_read + " waypoints!");
        }

        return nxtWpts;
    } 


   
    public waypoint getClosestWaypoint(Transform transform) {

        float w_distance = 100000000000000;
        float prev_distance = 100000000000000;

        float d_distance_d_t = 0;
        float prev_d_distance_d_t = 0;

        float d_d_distance_d_t = 0;
        int prev_d_distance_sign = 0;
        
        int indx = 0;
      
        waypoint c_w = target_path_waypoints_set_first;       
        while (c_w != null) {
            
            w_distance = distance(c_w,new waypoint(transform.position));
            d_distance_d_t = w_distance-prev_distance;
            d_d_distance_d_t = d_distance_d_t - prev_d_distance_d_t;


            if (w_distance < distBetweenWaypoints ) {
                if (d_distance_d_t > 1 && d_d_distance_d_t > 1) {
                    break;
                }
            }
            

            indx++;
            c_w = c_w.next;

            prev_distance = w_distance;
            prev_d_distance_d_t = d_distance_d_t;

            if (d_distance_d_t < 0) {
                prev_d_distance_sign = -1;
            }else if (d_distance_d_t > 0 ) {
                prev_d_distance_sign = 1;
            }
        }

        Debug.Assert(w_distance < distBetweenWaypoints);
        Debug.Assert(c_w != null);

        Debug.Log("ClosestPoint to " + transform.position + " was " + c_w.to_str());

        return c_w;
    }

}
}