using UnityEngine;
using System.Collections;


namespace UnityStandardAssets.Vehicles.Car
{
    [RequireComponent(typeof(CarController))]
    public class AI_CarControl : MonoBehaviour
    {

        public bool isPlayer;

        public bool randomDriving;

        public bool wavyRandomDriving;
        public int randomDrivingLoopsBeforeLaneChange = 1000;

        private CarController m_Car;
        private Steering s;

        [Header("Sensors")]
        public float sensorLength = 30f;
        public Vector3 frontSensorPosition = new Vector3(0f, 0.2f, 0.5f);

        public Vector3 sideSensorPosition = new Vector3(0f,0f,0f);
        public float frontSideSensorPosition = 0.2f;
        public float frontSensorAngle = 30f;

        public float sidePersonalSpaceMargin = 10f;

        public float carWidth = 20;

        private float maxSteerAngle;

        public float minSteerAngle = 0f;

        public float maxPidTargetChangeInOneFrame = 0.2f;
        private float targetSteerAngle = 0;

        private float prevError = 1, PIDOutput = 0, pidTarget = 0f, prevCenterDistance = -1, prevTargPos = 0, prevPidTargError = 0;

        private float leftDist = 30, frontLeftAngleDist = 30, centerDist = 30, frontRightAngleDist = 30, rightDist = 30; //the readings of all the sensors

        private float throttle = 1, brake = 0;

        private bool objectInfront = false, passing = false, attenuatePIDTarget = true;

        private long loops = 0;
        private float swerveDirection = -1.0f;
        private float trackWidth = 0;

        private int waypointIndx = 0;

        private int totalWayPointIndx = 0; //used to make sure we put new waypoints in the right spots when we recieve new waypoints

        public float[][] waypoints = {new float[] {-25,17}, new float[] {-25,204}, new float[] {-25,400}, new float[] {-25,571}, new float[] {-31.6f,605.7f}, new float[] {-47.9f,644.5f}, new float[] {-74.4f,677.7f}, new float[] {-109,703.6f}, new float[] {-149.4f,718.2f}, new float[] {-186,724}, new float[] {-327.6f,724}, new float[] {-367.2f,718.2f}, new float[] {-408,701.2f}, new float[] {-440.2f,678.4f}, new float[] {-464.6f,650.5f}, new float[] {-483.2f,613.8f}, new float[] {-492.2f,565.8f}, new float[] {-493.5f,400}, new float[] {-493.5f,204}, new float[] {-493.5f,17}, new float[] {-493.5f,-120}, new float[] {-487.5f,-165}, new float[] {-473.5f,-200.7f}, new float[] {-450.6f,-232.9f}, new float[] {-425.2f,-255.2f}, new float[] {-395.6f,-272.6f}, new float[] {-361.9f,-283.6f}, new float[] {-344.3f,-286.6f}, new float[] {-180,-286.2f}, new float[] {-138.7f,-278.2f}, new float[] {-104.6f,-263.6f}, new float[] {-75,-240.4f}, new float[] {-51,-211.7f}, new float[] {-32.5f,-172.6f}, new float[] {-25,-120}};

        private float randPidTargetSet = 0f;

        public float timeSpeed = 2.0f;

        public bool useWaypoints = false;


        public TrainingData_collection_controller _TrainingData_collection_controller;


        private void Awake()
        {

            m_Car = GetComponent<CarController>();

            //Custom init
            maxSteerAngle = m_Car.m_MaximumSteerAngle;


            s = new Steering();
            s.Start();


            Time.timeScale = timeSpeed;


        }

        private void FixedUpdate()
        {
            double outputSteeringAngle = 0;

            s.UpdateValues();

            if (isPlayer) {

                if (useWaypoints) {
                    outputSteeringAngle = PIDWaypointsV1();
                }else{
                    outputSteeringAngle = SensorsV4(false,0f); //false,0.5f
                }

            }else{
                outputSteeringAngle = SensorsV4(false,0f);
            }


            // move function declaration: public void Move (float steering, float accel, float footbrake, float handbrake)

            m_Car.Move(targetSteerAngle, throttle, brake, brake);
            //m_Car.Move(s.H, s.V, s.V, 0f);

        }

        private void incrementWaypoint() {
            totalWayPointIndx++;
            waypointIndx++;
            Debug.Log("Waypoint changed");

            if (waypointIndx >= waypoints.Length) { //if we have reached all the waypoints...
                waypointIndx = 0; //assume we are going around a circular track
                Debug.Log("Lap Completed, returning to first waypoint");
            }
        }


            private float PIDWaypointsV1() {
                Vector3 nxt = _TrainingData_collection_controller.getNextWaypoints(transform,2)[1];
                float[] devs = getDeviationFromWayPoint(new float[] {nxt.x, nxt.y, nxt.z}); //get the waypoint bearing information from the current target waypoint

                float waypointDistance = devs[0];
                float angleDeviation = devs[1];
                float lateralDeviation = devs[2];
                float dotProduct = devs[3];

                //switch to the next waypoint when the waypoint we are at is too close
                int loops = 0;
                while (dotProduct < 20 && loops < 20) {
                    incrementWaypoint();
                    devs = getDeviationFromWayPoint(getCurrentWaypoint());
                    dotProduct = devs[3];
                    loops++;
                }



                /* IMPORTANT: the lateal deviation will fight the angle deviation to smooth out our curves inbetween waypoints.
                * it is NOT pushing us towards the waypoint it is pushing us away from it.
                * Since the lateral deviation stays the same as we get closer, and the angle error will increase as we get closer,
                * The combination of the two will result in a smooth curve.
                *
                * This also has the effect of natrually making sure that the car never turns too sharply and looses control
                */

                // Note that this is a delicate balance - the porportion of these two terms will roughly equate to the radius of curvature for turns.
                // the higher lateral deviation is the wider the turns are
                float error = angleDeviation - 0.65f*lateralDeviation;



                //pid controller
                PIDOutput = 0.1f*prevError + 0.25f * (error-prevError);

                prevError = error;

                //set the output of the pid controller to targetSteerAngle
                if ( Mathf.Abs(maxSteerAngle * PIDOutput) < minSteerAngle) {
                    //if we are turning less than the min steering angle, don't turn at all
                    targetSteerAngle = 0;
                }else{
                    //otherwise output normally
                    targetSteerAngle = maxSteerAngle * PIDOutput;
                }

                return targetSteerAngle;
            }


            public float[] getCurrentWaypoint() {
                return waypoints[waypointIndx];
            }

 /**
            * Returns information about the bearings of the car with respect to the passed waypoint in the form {x,z}
            *
            * returns an array of floats:
            * [0] = distanceToWaypoint - the distance from the car to the waypoint
            * [1] = angle - the angle between the car and the waypoint in RADIANS
            * [2] = lateralDistance
            * [3] = dotProduct - the dot product between waypointVec and Transform.forward - how far infront of us the waypoint is
            *
            **/
            private float[] getDeviationFromWayPoint(float[] wayPoint) {
                float angle, lateralDistance = 0, dotProduct;

                Vector3 wayPointVec = new Vector3(wayPoint[0] - transform.position.x, 0, wayPoint[2] - transform.position.z); //vector pointing from the car to the waypoint
                Vector3 carDirectionVec = new Vector3(transform.forward.x, 0, transform.forward.z); // vector pointing in the same direction as the car

                // use the magnitude of the wayPointVec to get the distance between the car and the waypoint
                float distanceToWaypoint = wayPointVec.magnitude;

                //now normalize both vectors for angle calculations (now they both have magnitude 1)
                wayPointVec.Normalize();
                carDirectionVec.Normalize();

                float angleSign = Vector3.Cross(carDirectionVec,wayPointVec).y; // use a cross product to determing which side of the car the waypoint is on
                angleSign /= Mathf.Abs(angleSign); //make angleSign 1 or -1


                //calculate the dot product. the sign of this will be whether the waypoint is infront of us
                dotProduct = Vector3.Dot(wayPointVec,carDirectionVec);


                angle = angleSign * Mathf.Acos(dotProduct); // calculating the angle between the vectors by taking the dot product equation and solving for theta


                /* IMPORTANT: the lateral deviation will fight the angle deviation to smooth out our curves inbetween waypoints.
                * it is NOT pushing us towards the waypoint it is pushing us away from it.
                * Since the lateral deviation stays the same as we get closer, and the angle error will increase as we get closer,
                * The combination of the two will result in a smooth curve.
                *
                * This also has the effect of natrually making sure that the car never turns too sharply and looses control
                */

                //calculate the lateral distance if the angle < 90 (otherwise this algorithm would not work)
                lateralDistance = -angleSign * Mathf.Sin(angle);

                return new float[] {distanceToWaypoint,angle,lateralDistance,dotProduct};
            }




        /**
        * returns a double value between -1 and 1 representing the lateral position of the car on the track
        * -1 is all the way to the left of the track, 0 is on the center line, and 1 is all the way to the right
        **/
        private float getRelativeCarPosition() {

            RaycastHit hit;
            Vector3 sensorStartPos = transform.position;
            sensorStartPos += transform.forward * frontSensorPosition.z;
            sensorStartPos += transform.up * frontSensorPosition.y;

            Vector3 sideSensorPos = transform.position;
            sideSensorPos += transform.forward * sideSensorPosition.z;
            sideSensorPos += transform.up * sideSensorPosition.y;

            float position = 0;

             // find the position of the car



    // find the position of the car

        //get ray distance data

            //front right angle sensor
            if (Physics.Raycast(sensorStartPos, Quaternion.AngleAxis(frontSensorAngle, transform.up) * transform.forward, out hit, sensorLength)) {
                if (!hit.collider.CompareTag("Terrain")) {
                    Debug.DrawLine(sensorStartPos, hit.point);
                    frontRightAngleDist = hit.distance;
                }
            }



            //front left angle sensor
            if (Physics.Raycast(sensorStartPos, Quaternion.AngleAxis(-frontSensorAngle, transform.up) * transform.forward, out hit, sensorLength)) {
                if (!hit.collider.CompareTag("Terrain")) {
                    Debug.DrawLine(sensorStartPos, hit.point);
                    frontLeftAngleDist = hit.distance;
                }
            }


            //leftSensor
            if (Physics.Raycast(sideSensorPos, Quaternion.AngleAxis(-90, transform.up) * transform.forward, out hit, sensorLength)) {
                if (!hit.collider.CompareTag("Terrain")) {
                    Debug.DrawLine(sensorStartPos, hit.point);
                    leftDist = hit.distance;
                }
            }

            //right sensor
            if (Physics.Raycast(sideSensorPos, Quaternion.AngleAxis(90, transform.up) * transform.forward, out hit, sensorLength)) {
                if (!hit.collider.CompareTag("Terrain")) {
                    Debug.DrawLine(sensorStartPos, hit.point);
                    rightDist = hit.distance;
                }
            }



            float frontFacingSensorLength = 2*sensorLength; //the front-facing sensors need to see further

            //front center sensor
            if (Physics.Raycast(sensorStartPos, transform.forward, out hit, frontFacingSensorLength)) {
                if (hit.collider.CompareTag("CPU")) {
                    Debug.DrawLine(sensorStartPos, hit.point);
                    if (hit.distance < centerDist) {
                        centerDist = hit.distance;
                    }
                }
            }

             //forward-facing right sensor
            sensorStartPos += transform.right * frontSideSensorPosition;
                //these sensors are angled in slightly so that they dont detect the walls while we are turning
            if (Physics.Raycast(sensorStartPos, Quaternion.AngleAxis(-2, transform.up) * transform.forward, out hit, frontFacingSensorLength)) {
                if (hit.collider.CompareTag("CPU")) {
                    Debug.DrawLine(sensorStartPos, hit.point);
                    if (hit.distance < centerDist) {
                        centerDist = hit.distance;
                    }
                }
            }


            //forward-facing left sensor
                //these sensors are angled in slightly so that they dont detect the walls while we are turning
            sensorStartPos -= transform.right * frontSideSensorPosition * 2;
            if (Physics.Raycast(sensorStartPos, Quaternion.AngleAxis(2, transform.up) * transform.forward, out hit, frontFacingSensorLength)) {
                if (hit.collider.CompareTag("CPU")) {
                    Debug.DrawLine(sensorStartPos, hit.point);
                    if (hit.distance < centerDist) {
                        centerDist = hit.distance;
                    }
                }
            }



            //Calculate width of track (average over the first 100 loops)
            if (loops < 300) {
                trackWidth *= loops-1;
                trackWidth += (leftDist+rightDist);
                trackWidth /= loops;

                position = frontRightAngleDist+rightDist-leftDist-frontLeftAngleDist;
            }else{


                position = 0;
                position += (frontRightAngleDist - frontLeftAngleDist);

                if (2*frontRightAngleDist*Mathf.Sin(frontSensorAngle) < trackWidth) {
                    position += (trackWidth-frontLeftAngleDist*Mathf.Sin(frontSensorAngle)) / Mathf.Sin(frontSensorAngle);
                }else{
                    position += frontRightAngleDist;
                }

                if (2*frontLeftAngleDist*Mathf.Sin(frontSensorAngle) < trackWidth) {
                    position -= (trackWidth-frontRightAngleDist*Mathf.Sin(frontSensorAngle)) / Mathf.Sin(frontSensorAngle);
                }else{
                    position -= frontLeftAngleDist;
                }


                if (rightDist < (trackWidth-leftDist) ) {
                    position += (trackWidth-leftDist);
                }else{
                    position += rightDist;
                }

                if (leftDist < (trackWidth-rightDist) ) {
                    position -= (trackWidth-rightDist);
                }else{
                    position -= leftDist;
                }

            }

            if (loops > 1000000000000000L) loops = 100000L; //make sure we don't exceed max range of long


            position /= trackWidth*4;

            return position;

        }


        /**
        * Drives car with raytracing on invisible walls
        * controlledTarget allows you to manually set the pid target if controlledDriving is true.
        */
        private double SensorsV4(bool controlledDriving, float controlledTarget) {
            /*
            Specs
            */

        //initialize
            //set up variables
            const float Kp = 0.05f;
            const float Kd = 0.6f;

            float position = 0, pError = 0;


            centerDist = 100000000000000;

            //increase loops for functions that change over time
            loops++;




            position = getRelativeCarPosition();



    //figure out what the pid target should be

            //figure out whether there is an object infront of the car
            if ((centerDist < frontLeftAngleDist*1.5) && (centerDist < frontRightAngleDist*1.5)) {
                objectInfront = true;
            }else{
                objectInfront = false;
            }


            //check if there is an object in front of us, and if so - swerve!
            if (objectInfront)  {
                pidTarget -= swerveDirection*maxPidTargetChangeInOneFrame;// (swerveDirection*1-pidTarget)/2;
                passing = true;
                attenuatePIDTarget = false;

                // slow down to try and match the car infront of us's speed
                if (prevCenterDistance != -1f) {
                    if (centerDist-prevCenterDistance < 5) {
                        throttle += (1f-throttle/2f);
                        brake /= 2f;
                    }else{
                        brake += (1f-brake)/2f;
                        throttle /= 2f;
                    }
                }else{
                    throttle = 0;
                    brake = 1;
                }
                prevCenterDistance = centerDist;
            }else{
                throttle = 1;
                brake = 0;
                prevCenterDistance = -1;
            }

            // if we are trying to swerve and we hit the edge of the track, try swerving the other way
            if ((objectInfront) && (Mathf.Abs(pidTarget) >= 0.9)) {
                swerveDirection *= -1f;
                pidTarget = 0.8999f * Mathf.Abs(pidTarget)/pidTarget;
            }


            //determine whether we should be driving randomly, falling back to the center, or neither
            if ( ((! passing) && randomDriving) || controlledDriving ) { //if we want to drive randomly
                if (randomDriving) {
                    if (wavyRandomDriving) {
                        float x = ((float) loops) / randomDrivingLoopsBeforeLaneChange;

                        pidTarget = Mathf.Sin(x) + Mathf.Sin(x*0.7f);
                        pidTarget /= 2.1f;

                    }else{
                        if (loops % randomDrivingLoopsBeforeLaneChange == 0) {
                            randPidTargetSet = Mathf.Sqrt(Random.Range(0.0f,1f));
                            randPidTargetSet *= Random.Range(0f,1f) < 0.5 ? -1f: 1f;
                        }
                        pidTarget = randPidTargetSet;
                    }

                }else if (controlledDriving) {
                    pidTarget = controlledTarget;
                }
            }else if (attenuatePIDTarget) {
                pidTarget /= 1.11f; // fall back towards the center line
            }
            if ( leftDist + rightDist <  0.9*trackWidth) { //we are beside something
                Debug.Log("not passing");
                passing = false;
            }else if (! passing) { // we have passed the thing we were passing
                attenuatePIDTarget = true;
            }


            // check if there is something too close beside us
            if (! objectInfront) {
                if (leftDist/(trackWidth/2) < sidePersonalSpaceMargin) {
             //       Debug.Log("Object too close on left");
                    pidTarget -= 0.2f * (sidePersonalSpaceMargin-leftDist/(trackWidth/2));
                }
                if (rightDist/(trackWidth/2) < sidePersonalSpaceMargin) {
            //        Debug.Log("Object too close on right");
                    pidTarget += 0.2f * (sidePersonalSpaceMargin-rightDist/(trackWidth/2));
                }
            }


    // PID controller - determine the steering angle


            float pidTargPos; // convert the -1 to 1 float of pidTarget to an actual location on the track
            float pidTargError = pidTarget-prevTargPos;



            if (loops < 310) { //give the car time to speed up before we change lanes
                pidTargPos = 0;
            }else{
                pidTargPos = prevTargPos + 0.02f*(pidTargError) + 0.1f*(pidTargError-prevPidTargError); //0.05f*pError + 0.6f * (error-prevError);
                prevPidTargError = pidTargError;
            }



            // make sure we dont change the pid target too much in one frame (which will cause the car to lose control)
            if (Mathf.Abs(prevTargPos-pidTargPos) > maxPidTargetChangeInOneFrame) {
            //    Debug.Log("capped pid target change");
                pidTarget = prevTargPos + maxPidTargetChangeInOneFrame * Mathf.Abs(prevTargPos-pidTarget)/(pidTarget-prevTargPos);
            }

            float error = position-pidTargPos; //the distance we are away from where we want to be


            // if our error is too big the pid will try and turn the car so rapidly we lose control. Cap the error so that doesn't happen
            if (Mathf.Abs(error) > 3*maxPidTargetChangeInOneFrame) {
                pError = 2*maxPidTargetChangeInOneFrame * error/Mathf.Abs(error);
            }else{
                pError = error;
            }

            //pid controller
            PIDOutput = Kp*pError + Kd * (error-prevError);

            //set the output of the pid controller to
            if ( Mathf.Abs(maxSteerAngle * PIDOutput) < minSteerAngle) {
                //if we are turning less than the min steering angle, don't turn at all
                targetSteerAngle = 0;
            }else{
                //otherwise output normally
                targetSteerAngle = maxSteerAngle * PIDOutput;
            }


            prevError = error; // set the previous error to this loop's error   --   for derivative terms and for capping the max error
            prevTargPos = pidTarget;

            return targetSteerAngle;
        }

        private double SensorsV3() { //also old

            /*
            Specs:
            cruising speed: 100mph
            max speed: 150+ mph
            Avoidance: Straights and turns with speed 15 < dif < 35 mph
            */

        //initialize
            //set up variables
            RaycastHit hit;
            Vector3 sensorStartPos = transform.position;
            sensorStartPos += transform.forward * frontSensorPosition.z;
            sensorStartPos += transform.up * frontSensorPosition.y;

            float position = 0, pError = 0;

            //increase loops for functions that change over time
            loops++;


        //get ray distance data
            //front right sensor
            sensorStartPos += transform.right * frontSideSensorPosition;
            if (Physics.Raycast(sensorStartPos, transform.forward, out hit, sensorLength)) {
                if (!hit.collider.CompareTag("Terrain")) {
                    Debug.DrawLine(sensorStartPos, hit.point);
                    rightDist = hit.distance;
                }
            }

            //front right angle sensor
            else if (Physics.Raycast(sensorStartPos, Quaternion.AngleAxis(frontSensorAngle, transform.up) * transform.forward, out hit, sensorLength)) {
                if (!hit.collider.CompareTag("Terrain")) {
                    Debug.DrawLine(sensorStartPos, hit.point);
                    frontRightAngleDist = hit.distance;
                }
            }

            //front left sensor
            sensorStartPos -= transform.right * frontSideSensorPosition * 2;
            if (Physics.Raycast(sensorStartPos, transform.forward, out hit, sensorLength)) {
                if (!hit.collider.CompareTag("Terrain")) {
                    Debug.DrawLine(sensorStartPos, hit.point);
                    leftDist = hit.distance;
                }
            }

            //front left angle sensor
            else if (Physics.Raycast(sensorStartPos, Quaternion.AngleAxis(-frontSensorAngle, transform.up) * transform.forward, out hit, sensorLength)) {
                if (!hit.collider.CompareTag("Terrain")) {
                    Debug.DrawLine(sensorStartPos, hit.point);
                    frontLeftAngleDist = hit.distance;
                }
            }

            //front center sensor
            float centerSensorLength = 2*sensorLength;// sensorLength*Mathf.Sin(frontSensorAngle); //make sure that angled sensors detect curves before we do

            if (Physics.Raycast(sensorStartPos, transform.forward, out hit, centerSensorLength)) {

                if (!hit.collider.CompareTag("Terrain")) {
                    Debug.DrawLine(sensorStartPos, hit.point);

                    centerDist = hit.distance;

                    if ((centerDist < leftDist) && (centerDist < rightDist)) {
                        objectInfront = true;
                    }else{
                        objectInfront = false;
                    }
                }else{
                    objectInfront = false;
                    centerDist = -1f;
                    prevCenterDistance = -1f;
                }
            }else{ //there is no object within our range
                objectInfront = false;
                centerDist = -1f;
                prevCenterDistance = -1f;
            }

            //Calculate width of track
            if (loops < 50) {
                trackWidth *= loops-1;
                trackWidth += (leftDist+rightDist);
                trackWidth /= loops;
            }



            // check if there is a car infront of us
            if (objectInfront)  {
                pidTarget -= (swerveDirection*1-pidTarget)/2; //swerveDirection*maxPidTargetChangeInOneFrame;
                passing = true;
                attenuatePIDTarget = false;
                if (prevCenterDistance != -1f) {
                    if (centerDist-prevCenterDistance < 0) {
                        throttle += (1f-throttle/2f);
                        brake /= 2f;
                    }else{
                        brake += (1f-brake)/2f;
                        throttle /= 2f;
                    }
                }else{
                    throttle = 0;
                    brake = 1;
                }
                prevCenterDistance = centerDist;


            }else{
                throttle = 1;
                brake = 0;
                prevCenterDistance = -1;
            }


            // check if there is something too close beside us
            if (leftDist < sidePersonalSpaceMargin) {
                pidTarget -= 1f * (sidePersonalSpaceMargin-leftDist)/(trackWidth/2);
            }
            if (rightDist < sidePersonalSpaceMargin) {
                pidTarget += 1f * (sidePersonalSpaceMargin-rightDist)/(trackWidth/2);
            }

            position = frontRightAngleDist+rightDist-leftDist-frontLeftAngleDist;


            if (Mathf.Abs(prevTargPos-pidTarget) > maxPidTargetChangeInOneFrame) {
                Debug.Log("capped pid target change");
                pidTarget = prevTargPos + maxPidTargetChangeInOneFrame * Mathf.Abs(prevTargPos-pidTarget)/(pidTarget-prevTargPos);
            }

            if ((objectInfront) && (Mathf.Abs(pidTarget) >= 0.9)) {
                swerveDirection *= -1f;
                pidTarget = 0.8999f * Mathf.Abs(pidTarget)/pidTarget;
            }


            if ((! passing) && randomDriving) { //if we want to drive randomly


                if (loops % 1000 == 0) {
                    randPidTargetSet = Mathf.Sqrt(Random.Range(0.0f,1f));
                    randPidTargetSet *= Random.Range(0f,1f) < 0.5 ? -1: 1;
                    Debug.Log("Random target set to: " + randPidTargetSet);
                }
                pidTarget = randPidTargetSet;
            }else if (attenuatePIDTarget) {
                pidTarget /= 1.01f; //fall back to the center
            }else if ( Mathf.Abs((leftDist + rightDist) - trackWidth) < 0) { //we are beside something
                passing = false;
            }else if (! passing) { // we have passed the thing we were passing
                swerveDirection *= -1f;
                attenuatePIDTarget = true;
            }

            if (loops > 1000000000000000L) loops = 100000L; //make sure we don't exceed max range of long

            prevTargPos = pidTarget;

            float pidTargPos = pidTarget * (trackWidth/2);


            float error = position-pidTargPos;

            if (Mathf.Abs(error) > maxPidTargetChangeInOneFrame) {
                pError = maxPidTargetChangeInOneFrame * error/Mathf.Abs(error);
            }

            //pid controller
            PIDOutput = 0.1f*pError + 0.3f * (error-prevError);

            if ( Mathf.Abs(maxSteerAngle * PIDOutput) < minSteerAngle) {
                targetSteerAngle = 0;
            }else{
                targetSteerAngle = maxSteerAngle * PIDOutput;
            }


            prevError = error;

            return targetSteerAngle;


        }


        private double SensorsV2()  {//old verson of auton

            /* SPECS:
                Cruising speed: 100mph
                Max speed: 120 mph
            */

            RaycastHit hit;
            Vector3 sensorStartPos = transform.position;
            sensorStartPos += transform.forward * frontSensorPosition.z;
            sensorStartPos += transform.up * frontSensorPosition.y;
            float avoidMultiplier = 0;

            //front right sensor
            sensorStartPos += transform.right * frontSideSensorPosition;
            if (Physics.Raycast(sensorStartPos, transform.forward, out hit, sensorLength)) {
                if (!hit.collider.CompareTag("Terrain")) {
                    Debug.DrawLine(sensorStartPos, hit.point);
                    avoidMultiplier -= 10f/hit.distance;
                }
            }

            //front right angle sensor
            else if (Physics.Raycast(sensorStartPos, Quaternion.AngleAxis(frontSensorAngle, transform.up) * transform.forward, out hit, sensorLength)) {
                if (!hit.collider.CompareTag("Terrain")) {
                    Debug.DrawLine(sensorStartPos, hit.point);
                    avoidMultiplier -= 0.5f/hit.distance;
                }
            }

            //front left sensor
            sensorStartPos -= transform.right * frontSideSensorPosition * 2;
            if (Physics.Raycast(sensorStartPos, transform.forward, out hit, sensorLength)) {
                if (!hit.collider.CompareTag("Terrain")) {
                    Debug.DrawLine(sensorStartPos, hit.point);
                    avoidMultiplier += 10f/hit.distance;
                }
            }

            //front left angle sensor
            else if (Physics.Raycast(sensorStartPos, Quaternion.AngleAxis(-frontSensorAngle, transform.up) * transform.forward, out hit, sensorLength)) {
                if (!hit.collider.CompareTag("Terrain")) {
                    Debug.DrawLine(sensorStartPos, hit.point);
                    avoidMultiplier += 0.5f/hit.distance;
                }
            }

            //front center sensor
            if (avoidMultiplier == 0) {
                if (Physics.Raycast(sensorStartPos, transform.forward, out hit, sensorLength)) {
                    if (!hit.collider.CompareTag("Terrain")) {
                        Debug.DrawLine(sensorStartPos, hit.point);
                        if (hit.normal.x < 0) {
                            avoidMultiplier = -0.001f;
                        } else {
                            avoidMultiplier = 0.001f;
                        }
                    }
                }
            }


            PIDOutput = 0.2f*(avoidMultiplier) + 1.2f * (avoidMultiplier-prevError);

            targetSteerAngle = maxSteerAngle * PIDOutput;

            prevError = avoidMultiplier;

            return targetSteerAngle;

        }


    }
}
