﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AI_Control2 : MonoBehaviour
{

    void Start()
    {
        SelectRandomPointOnTrack();
    }

    void Update()
    {
        
    }

    void SelectRandomPointOnTrack()
    {
        Vector3 center = new Vector3(-250, 1, 250); // y doesn't matter
        Vector3 randPoint = new Vector3(Random.Range(-1.0f,1.0f), 0, Random.Range(-1.0f,1.0f));
        Vector3 innerPoint = new Vector3(0,0,0);
        Vector3 outerPoint = new Vector3(0,0,0);
        RaycastHit hit;

        int innerMask = 1 << 8;
        int outerMask = 1 << 9;

        Debug.DrawLine(center, center + randPoint * 700);

        if (Physics.Raycast(center, randPoint, out hit, 700, innerMask) ) {
            innerPoint = hit.point;
        }

        if (Physics.Raycast(center, randPoint, out hit, 700, outerMask) ) {
            outerPoint = hit.point;
        }

        Debug.Log(innerPoint);
        Debug.Log(outerPoint);
        Debug.DrawLine(innerPoint, outerPoint);

    }
}


