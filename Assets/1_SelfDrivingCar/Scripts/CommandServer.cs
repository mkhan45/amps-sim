﻿using UnityEngine;
using System.Collections.Generic;
using System.Collections;
using SocketIO;
using UnityStandardAssets.Vehicles.Car;
using System;
using System.Security.AccessControl;

public class CommandServer : MonoBehaviour
{
	public PIDWaypointCarController CarWaypointController;
	public Camera FrontFacingCamera;
	private SocketIOComponent _socket;
	private CarController _carController;

	private Transform carPositionOnMostRecentCall = null;
	private int totalWaypointIndxOnMostRecentCall = 0;

	// Use this for initialization
	void Start()
	{
		_socket = GameObject.Find("SocketIO").GetComponent<SocketIOComponent>();
		_socket.On("open", OnOpen);
		_socket.On("steer", OnSteer);
		_socket.On("manual", onManual);
		_carController = CarWaypointController.GetComponent<CarController>();
	}

	// Update is called once per frame
	void Update()
	{
	}

	void OnOpen(SocketIOEvent obj)
	{
		Debug.Log("Connection Open");
		Debug.Log("Goose do you read me?");
		EmitTelemetry(obj);
	}

	// 
	void onManual(SocketIOEvent obj)
	{
		EmitTelemetry (obj);
	}

	void OnSteer(SocketIOEvent obj)
	{

		
		try{
		JSONObject jsonObject = obj.data;
		//    print(float.Parse(jsonObject.GetField("steering_angle").str));
	//	CarRemoteControl.SteeringAngle = float.Parse(jsonObject.GetField("steering_angle").str);
	//	CarRemoteControl.Acceleration = float.Parse(jsonObject.GetField("throttle").str);

	
		
		string waypointsStr = jsonObject.GetField("waypoints").str;
		waypointsStr = waypointsStr.Replace("\\n",null);
		waypointsStr = waypointsStr.Replace("[",null);
		waypointsStr = waypointsStr.Replace("]",null);

		Debug.Log("waypoints recieved:  " + waypointsStr);


		string[] waypointsList = waypointsStr.Split(',');

	//	Debug.Log("waypointslist: ");

		string w_list = "";
		for (int i = 0; i < waypointsList.Length; i++) {
			w_list += waypointsList[i];
			w_list += ",";
		}

	//	Debug.Log(w_list);

		float[][] waypoints = new float[waypointsList.Length/3][];





		int waypointIndx = 0;
	
		for (int w_num = 0; w_num < waypoints.Length && waypointIndx < waypointsList.Length; w_num++) {
	
			waypoints[w_num] = new float[3];
			for (int cord_indx = 0; cord_indx < 3 && waypointIndx < waypointsList.Length; cord_indx++) {
//				Debug.Log(waypointsList[w_num*3+cord_indx]);
				if (waypointsList[w_num*3+cord_indx].Length == 0) {
					Debug.Log("Skipping empty string");
					cord_indx--;
				}else{
					waypoints[w_num][cord_indx] = float.Parse(waypointsList[w_num*3+cord_indx]);
					
				}

				waypointIndx++; //count the index of the recieved strings seperately so we can skip if needed
			}
		}
		
	//	Debug.Log("waypoints Successfully parsed  " );
/*
		string waypoints_out_string = ""; 

		for (int w_num = 0; w_num < waypoints.Length; w_num++) {
			for (int cord_indx = 0; cord_indx < 3; cord_indx++ ) {
				waypoints_out_string += waypoints[w_num][cord_indx];
				waypoints_out_string += " ";
			}
			waypoints_out_string += ",";
		}

		Debug.Log("Parsed: " + waypoints_out_string);
*/
		CarWaypointController.addLocalWaypoints(waypoints,totalWaypointIndxOnMostRecentCall, carPositionOnMostRecentCall);
		
		
		
		EmitTelemetry(obj);
		

		}catch(Exception e) {
			Debug.Log(e.ToString());
			EmitTelemetry(obj);
		}
	}

	void EmitTelemetry(SocketIOEvent obj)
	{
		UnityMainThreadDispatcher.Instance().Enqueue(() =>
		{
			
			
			
			print("Attempting to Send...");


			carPositionOnMostRecentCall = _carController.getCarTransform();
			totalWaypointIndxOnMostRecentCall = CarWaypointController.getTotalWaypointIndex();

			// send only if it's not being manually driven
			if ((Input.GetKey(KeyCode.W)) || (Input.GetKey(KeyCode.S))) {
				_socket.Emit("telemetry", new JSONObject());
			}
			else {
				// Collect Data from the Car
				
				Dictionary<string, string> data = new Dictionary<string, string>();
				data["steering_angle"] = _carController.CurrentSteerAngle.ToString("N4");
				data["throttle"] = _carController.AccelInput.ToString("N4");
				data["speed"] = _carController.CurrentSpeed.ToString("N4");
				data["image"] = Convert.ToBase64String(CameraHelper.CaptureFrame(FrontFacingCamera));
				_socket.Emit("telemetry", new JSONObject(data));
			}
		});

		//    UnityMainThreadDispatcher.Instance().Enqueue(() =>
		//    {
		//      	
		//      
		//
		//		// send only if it's not being manually driven
		//		if ((Input.GetKey(KeyCode.W)) || (Input.GetKey(KeyCode.S))) {
		//			_socket.Emit("telemetry", new JSONObject());
		//		}
		//		else {
		//			// Collect Data from the Car
		//			Dictionary<string, string> data = new Dictionary<string, string>();
		//			data["steering_angle"] = _carController.CurrentSteerAngle.ToString("N4");
		//			data["throttle"] = _carController.AccelInput.ToString("N4");
		//			data["speed"] = _carController.CurrentSpeed.ToString("N4");
		//			data["image"] = Convert.ToBase64String(CameraHelper.CaptureFrame(FrontFacingCamera));
		//			_socket.Emit("telemetry", new JSONObject(data));
		//		}
		//      
		////      
		//    });
	}
}