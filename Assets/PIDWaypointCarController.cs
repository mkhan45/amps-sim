﻿/*


FILE DETAILS:

This file is a replacement for CarRemoteControl that runs the car based off of waypoints for the PID
rather than direct steering commands from the net.



*/






using System;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;


namespace UnityStandardAssets.Vehicles.Car
{
    [RequireComponent(typeof(CarController))]

    /**
    * This class replaces CarRemoteControl
    **/
    public class PIDWaypointCarController : MonoBehaviour {

            private CarController m_Car;
            private Steering s;


            private float prevError = 1, PIDOutput = 0, pidTarget = 0f;

            private float targetSteerAngle = 0;


            private float maxSteerAngle;

            public float minSteerAngle = 0f;


            private float throttle = 1, brake = 0;

            private int waypointIndx = 0;

            private int totalWayPointIndx = 0; //used to make sure we put new waypoints in the right spots when we recieve new waypoints

            public float[][] waypoints = {new float[] {-25,0,17}, new float[] {-25,0,204}, new float[] {-25,0,400}, new float[] {-25,0,571}};//{new float[] {-25,0,17}, new float[] {-25,0,204}, new float[] {-25,0,400}, new float[] {-25,0,571}, new float[] {-31.6f,0,605.7f}, new float[] {-47.9f,0,644.5f}, new float[] {-74.4f,0,677.7f}, new float[] {-109,0,703.6f}, new float[] {-149.4f,0,718.2f}, new float[] {-186,0,724}, new float[] {-327.6f,0,724}, new float[] {-367.2f,0,718.2f}, new float[] {-408,0,701.2f}, new float[] {-440.2f,0,678.4f}, new float[] {-464.6f,0,650.5f}, new float[] {-483.2f,0,613.8f}, new float[] {-492.2f,0,565.8f}, new float[] {-493.5f,0,400}, new float[] {-493.5f,0,204}, new float[] {-493.5f,0,17}, new float[] {-493.5f,0,-120}, new float[] {-487.5f,0,-165}, new float[] {-473.5f,0,-200.7f}, new float[] {-450.6f,0,-232.9f}, new float[] {-425.2f,0,-255.2f}, new float[] {-395.6f,0,-272.6f}, new float[] {-361.9f,0,-283.6f}, new float[] {-344.3f,0,-286.6f}, new float[] {-180,0,-286.2f}, new float[] {-138.7f,0,-278.2f}, new float[] {-104.6f,0,-263.6f}, new float[] {-75,0,-240.4f}, new float[] {-51,0,-211.7f}, new float[] {-32.5f,0,-172.6f}, new float[] {-25,0,-120}};


            public int getTotalWaypointIndex() => totalWayPointIndx;

            private void Awake()
            {

                m_Car = GetComponent<CarController>();

                //Custom init
                maxSteerAngle = m_Car.m_MaximumSteerAngle;


                s = new Steering();
                s.Start();


            }

            private void FixedUpdate()
            {
                double outputSteeringAngle = 0;

                outputSteeringAngle = PIDWaypointsV1();

                // move function declaration: public void Move (float steering, float accel, float footbrake, float handbrake)


                if (Input.GetKey(KeyCode.W) || Input.GetKey(KeyCode.S)) {
                    s.UpdateValues();
                    m_Car.Move(s.H, s.V, s.V, 0f);
                } else {
				    m_Car.Move(targetSteerAngle, throttle, brake, brake);
                }

            }


            /**
            * Transforms local waypoitns to global waypoints, then calls addGlobalWaypoints() to add them to the waypoints list
            * * This method is called with the output of the neural net. If this is called it is assumed that it will be constantly updated faster than we pass the waypoints.
            *
            * Params:
            * waypoints: the list of waypoints to add
            * waypointIndx: the value of totalWaypointIndx when this call was made - used as a timestamp to know where to add the waypoints
            * transform: Transform represention the position and orientation of the car
            **/
            public void addLocalWaypoints(float[][] inWaypoints, int inputTotalwaypointIndx, Transform transform) {
                
                inputTotalwaypointIndx = getTotalWaypointIndex(); //just input them where we are right now
                // current_waypoint[0] = x value
                // current_waypoint[1] = y value
                // current_waypoint[2] = z value

                Vector3 carPosition = transform.position;
                Vector3 carOrientation = transform.forward;


                Vector3 waypointVec; // Vector3 representation of waypoint

                foreach (float[] current_waypoint in inWaypoints) {
                    try {
                        //create a vector with this waypoint's values
                        waypointVec = new Vector3(current_waypoint[0],current_waypoint[1],current_waypoint[2]);


                        //translate relative to the car (from local to world)
                        waypointVec = transform.TransformPoint(waypointVec);


                        // set this waypoint's values to the new transformed posiiton
                        current_waypoint[0] = waypointVec.x;
                        current_waypoint[1] = waypointVec.y;
                        current_waypoint[2] = waypointVec.z;
                    }catch(NullReferenceException n) {
                        Debug.Log("Threw away: " + n.ToString());
                    }

                }

                addGlobalWaypoints(inWaypoints,inputTotalwaypointIndx);
            }


            /**
            * NOTE: If this method is called it is assumed that it will be constantly updated faster than we pass the waypoints.
            *
            * Params:
            * waypoints: the list of waypoints to add
            * waypointIndx - the value of totalWaypointIndx when this call was made - used as a timestamp to know where to add the waypoints
            **/
            public void addGlobalWaypoints(float[][] inWaypoints, int inputTotalwaypointIndx) {

                // save the state of the waypoint iteration at the start.
                // if these change by the end of this method execution we will adjust accordingly
                int totalWayPointIndx = this.totalWayPointIndx;
                int waypointIndx = this.waypointIndx;

                // calculate the length of the waypoint list after we add the new ones and get rid of all waypoints we have already been to
                int end_waypoint_list_length = 0;
                end_waypoint_list_length -= inputTotalwaypointIndx-totalWayPointIndx; // subtract how many waypoints in the new list we have already passed
                Debug.Log("Threw away " + end_waypoint_list_length + " from input list due to delay between waypoint request and recieving");
                end_waypoint_list_length += inWaypoints.Length;

                Debug.Assert(end_waypoint_list_length > 0); // make sure we actually have waypoints to add

                float[][] newWaypoints = new float[end_waypoint_list_length][];



                // populate the new waypoint list with the old and new waypoints
                try {
                    int inputWaypointsIndx;
                    for (int newWaypointsIndx = 0; newWaypointsIndx < end_waypoint_list_length; newWaypointsIndx++) {
                        inputWaypointsIndx = totalWayPointIndx - inputTotalwaypointIndx;

                        if (inputWaypointsIndx < 0) {
                            this.waypoints[waypointIndx + newWaypointsIndx] = inWaypoints[inputWaypointsIndx];
                        }else{
                            newWaypoints[newWaypointsIndx] = inWaypoints[inputWaypointsIndx];
                        }
                    }
                }catch(IndexOutOfRangeException i ) {
                    Debug.Log("Car is driving past waypoints faster than they are being resupplied! More points are needed in a smaller span of time.");
                    Debug.Log(i.StackTrace);
                }


                this.waypoints = newWaypoints;

                this.waypointIndx = this.totalWayPointIndx - totalWayPointIndx; // if we switched waypoints during the execution of this method start at the second waypoint rather than the first

            }


            private void incrementWaypoint() {
                totalWayPointIndx++;
                waypointIndx++;

                Debug.Log("Waypoint changed");

                if (waypointIndx >= waypoints.Length) { //if we have reached all the waypoints...
                    waypointIndx = 0; //assume we are going around a circular track
                    Debug.Log("Lap Completed, returning to first waypoint");
                }
            }

            public float[] getCurrentWaypoint() {
                return waypoints[waypointIndx];
            }


            private float PIDWaypointsV1() {

                float[] devs = getDeviationFromWayPoint(getCurrentWaypoint()); //get the waypoint bearing information from the current target waypoint

                float waypointDistance = devs[0];
                float angleDeviation = devs[1];
                float lateralDeviation = devs[2];
                float dotProduct = devs[3];

                //switch to the next waypoint when the waypoint we are at is too close
                int loops = 0;
                while (dotProduct < 20 && loops < 20) {
                    incrementWaypoint();
                    devs = getDeviationFromWayPoint(getCurrentWaypoint());
                    dotProduct = devs[3];
                    loops++;
                }



                /* IMPORTANT: the lateal deviation will fight the angle deviation to smooth out our curves inbetween waypoints.
                * it is NOT pushing us towards the waypoint it is pushing us away from it.
                * Since the lateral deviation stays the same as we get closer, and the angle error will increase as we get closer,
                * The combination of the two will result in a smooth curve.
                *
                * This also has the effect of natrually making sure that the car never turns too sharply and looses control
                */

                // Note that this is a delicate balance - the porportion of these two terms will roughly equate to the radius of curvature for turns.
                // the higher lateral deviation is the wider the turns are
                float error = angleDeviation - 0.65f*lateralDeviation;



                //pid controller
                PIDOutput = 0.1f*prevError + 0.25f * (error-prevError);

                prevError = error;

                //set the output of the pid controller to targetSteerAngle
                if ( Mathf.Abs(maxSteerAngle * PIDOutput) < minSteerAngle) {
                    //if we are turning less than the min steering angle, don't turn at all
                    targetSteerAngle = 0;
                }else{
                    //otherwise output normally
                    targetSteerAngle = maxSteerAngle * PIDOutput;
                }


                return targetSteerAngle;



            }

            

            /**
            * Returns information about the bearings of the car with respect to the passed waypoint in the form {x,z}
            *
            * returns an array of floats:
            * [0] = distanceToWaypoint - the distance from the car to the waypoint
            * [1] = angle - the angle between the car and the waypoint in RADIANS
            * [2] = lateralDistance
            * [3] = dotProduct - the dot product between waypointVec and Transform.forward - how far infront of us the waypoint is
            *
            **/
            private float[] getDeviationFromWayPoint(float[] wayPoint) {

                if (wayPoint == null) {
                    Debug.Log("Null waypoint!");
                    return new float[] {0,0,0,0};
                }else{
                    Debug.Log("successful waypoint");
                }
                float angle, lateralDistance = 0, dotProduct = 0;

                Vector3 wayPointVec = new Vector3(wayPoint[0] - transform.position.x, 0, wayPoint[2] - transform.position.z); //vector pointing from the car to the waypoint
                Vector3 carDirectionVec = new Vector3(transform.forward.x, 0, transform.forward.z); // vector pointing in the same direction as the car

                // use the magnitude of the wayPointVec to get the distance between the car and the waypoint
                float distanceToWaypoint = wayPointVec.magnitude;

                //now normalize both vectors for angle calculations (now they both have magnitude 1)
                wayPointVec.Normalize();
                carDirectionVec.Normalize();

                float angleSign = Vector3.Cross(carDirectionVec,wayPointVec).y; // use a cross product to determing which side of the car the waypoint is on
                angleSign /= Mathf.Abs(angleSign); //make angleSign 1 or -1


                //calculate the dot product. the sign of this will be whether the waypoint is infront of us
                dotProduct = Vector3.Dot(wayPointVec,carDirectionVec);


                angle = angleSign * Mathf.Acos(dotProduct); // calculating the angle between the vectors by taking the dot product equation and solving for theta


                /* IMPORTANT: the lateral deviation will fight the angle deviation to smooth out our curves inbetween waypoints.
                * it is NOT pushing us towards the waypoint it is pushing us away from it.
                * Since the lateral deviation stays the same as we get closer, and the angle error will increase as we get closer,
                * The combination of the two will result in a smooth curve.
                *
                * This also has the effect of natrually making sure that the car never turns too sharply and looses control
                */

                //calculate the lateral distance if the angle < 90 (otherwise this algorithm would not work)
                lateralDistance = -angleSign * Mathf.Sin(angle);

                return new float[] {distanceToWaypoint,angle,lateralDistance,dotProduct};
            }
    }
}
