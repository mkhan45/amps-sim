# Simulator

## Install Instructions for AMP

```
git clone https://github.com/mkhan45/self-driving-car-sim
```

Add the project through Unity; make sure you're using editor version 2019.x.x. There should be no compilation errors. If it asks to update the assets it works either way.

If you open the scene and there's a big white cube and no roads and everything just looks terrible, run:
```
git lfs fetch
git lfs install
git lfs checkout
```

This requires git lfs to be installed. AFAIK, the windows version of git comes with git lfs, but on Linux and probably MacOS you need to install it separately. 

## Building for AMP

1. Go to file > build settings 
2. Uncheck all the scenes except LauncherScene, LakeTrackTraining, LakeTrackAutonomous, MenuScene, and ControlMenu
3. Click Build

Building will take a few minutes and make your computer quite slow while doing so.

## Training/Running the model

1. After building, run the compiled program. 
2. On the menu, click training mode. 
3. Click the record button in the top right and choose a folder for the data to be stored in. You should choose the folder in which you cloned the pgebert repo. 
4. Click the record button again and drive around the track.
5. Click the stop recording button.
6. Go to your folder with the model repo and run `python model.py`. 
  - You'll probably have to import a few things, preemptively run: `pip install torch torchvision plotly --user`
  - There might be a few other things to import, for each of these run `pip install {{insert package name here}} --user`
  - You can stop this at any time. Just for testing run it for about 10 minutes.
